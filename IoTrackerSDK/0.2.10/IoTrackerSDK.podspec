Pod::Spec.new do |s|
  s.name = "IoTrackerSDK"
  s.version = "0.2.10"
  s.summary = "IoTracker release version"
  s.homepage = "https://bitbucket.org/technopartner/technotracker-ios-release"
  s.license = { :type => 'MIT', :file => 'LICENSE' }
  s.author = { "Technopartner" => "technopartner@technopartner.com.br" }

  s.source_files = 'IoTracker.framework/Headers/*.h'
  s.source = { :git => "git@bitbucket.org:technopartner/technotracker-ios-release.git", :tag => "#{s.version}" }

  s.public_header_files = "IoTracker.framework/Headers/*.h"
  s.vendored_frameworks = "IoTracker.framework"
  s.platform = :ios
  s.swift_versions = '5.0'

  s.ios.vendored_frameworks = "IoTracker.framework"
  s.ios.deployment_target = '12.0'
  s.ios.dependency 'RxBluetoothKit'
  s.ios.dependency 'CryptoSwift'
  s.ios.dependency 'KeychainSwift'
  
  s.pod_target_xcconfig = { 
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' 
  }
  s.user_target_xcconfig = { 
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
end