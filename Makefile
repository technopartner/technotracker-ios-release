add:
	@pod repo add IoTrackerSDK git@bitbucket.org:technopartner/technotracker-ios-release.git

publish:
	@pod repo push IoTrackerSDK IoTrackerSDK.podspec
	@git add --all && git commit -m "[skip ci] pod repo publish"
	@git pull
	@git push
