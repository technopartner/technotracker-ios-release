✨  )   8,@��
�P�P
�@
�0
� 
��
��
A�Ќ�+�-��+��/�+��(��C�B)�B(�B(�B(<0B+�)�+��/�B(�B)<-��,�B)��A�(�B+�B)�-��(��+��*<8)��(��/��+�B+��+��,<0�)��+�,��+�B+��, �	  �  %  "   Tf�� 0"��    �   Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)  L   IoTracker   �   arm64-apple-ios12.0     �  �  2J�I0-       @��7   i   s:9IoTracker20OpenLocationCodeAreaV14latitudeCenterSdvp&   The latitude of the center in degrees.      +   /// The latitude of the center in degrees.
        �l0�#   �  s:9IoTracker20OpenLocationCodeAreaV�   Coordinates of a decoded Open Location Code. The coordinates include the latitude and longitude of the lower left and upper right corners and the center of the bounding box for the area the code represents.      1   /// Coordinates of a decoded Open Location Code.
   M   /// The coordinates include the latitude and longitude of the lower left and
   L   /// upper right corners and the center of the bounding box for the area the
      /// code represents.
        �q�V     s:9IoTracker16OpenLocationCodeC10encodeGrid8latitude9longitude10codeLengthSSSd_SdSitFZ	  Encode a location using the grid refinement method into an OLC string. The grid refinement method divides the area into a grid of 4x5, and uses a single character to refine the area. This allows default accuracy OLC codes to be refined with just a single character.	      K   /// Encode a location using the grid refinement method into an OLC string.
   O   /// The grid refinement method divides the area into a grid of 4x5, and uses a
   J   /// single character to refine the area. This allows default accuracy OLC
   6   /// codes to be refined with just a single character.
      ///
   @   /// - Parameter latitude: A latitude in signed decimal degrees.
   B   /// - Parameter longitude: A longitude in signed decimal degrees.
   ?   /// - Parameter codeLength: The number of characters required.
   ?   /// - Returns: Open Location Code representing the given grid.
        L��Y3   o   s:9IoTracker20OpenLocationCodeAreaV10latitudeHiSdvp)   The latitude of the NE corner in degrees.      .   /// The latitude of the NE corner in degrees.
        ��(�9   �  s:9IoTracker16OpenLocationCodeC6decodeyAA0cdE4AreaVSgSSFZ�   Decodes an Open Location Code into the location coordinates. Returns a OpenLocationCodeArea object that includes the coordinates of the bounding box - the lower left, center and upper right.      A   /// Decodes an Open Location Code into the location coordinates.
   O   /// Returns a OpenLocationCodeArea object that includes the coordinates of the
   ;   /// bounding box - the lower left, center and upper right.
      ///
   8   /// - Parameter code: The Open Location Code to decode.
   M   /// - Returns: A CodeArea object that provides the latitude and longitude of
   H   ///   two of the corners of the area, the center, and the length of the
      ///   original code.
        Б��8   �   s:9IoTracker20OpenLocationCodeAreaV15longitudeCenterSdvp7   latitude_center: The latitude of the center in degrees.      <   /// latitude_center: The latitude of the center in degrees.
    	   P��(   �   s:9IoTracker22kDefaultFullCodeLengthSivpG   Default length of encoded Open Location Codes (not including + symbol).      L   /// Default length of encoded Open Location Codes (not including + symbol).
         T,3J0   �   s:9IoTracker9AvroValueO6encodeySays5UInt8VGSgSSF   AvroValue binary encoding.      �   /**
    AvroValue binary encoding.

    :param jsonSchema Avro JSON schema string.

    :returns Avro binary encoding as byte array. Nil if encoding fails.
    */        ����N   �   s:9IoTracker9AvroValueO6encode_6schemaSays5UInt8VGSgAA0C7EncoderC_AA6SchemaOtF   AvroValue binary encoding.      �   /**
    AvroValue binary encoding.

    :param encoder Avro Encoder.
    :param schema Avro schema object.

    :returns Avro binary encoding as byte array. Nil if encoding fails.
    */        �(�c3   o   s:9IoTracker20OpenLocationCodeAreaV10latitudeLoSdvp)   The latitude of the SW corner in degrees.      .   /// The latitude of the SW corner in degrees.
        ׉ 5-   q   s:9IoTracker27kDefaultShortCodeTruncationSivp*   Default truncation amount for short codes.      /   /// Default truncation amount for short codes.
        N@�4   x  s:9IoTracker16OpenLocationCodeC7isValid4codeSbSS_tFZ�   Determines if a code is valid. To be valid, all characters must be from the Open Location Code character set with at most one separator. The separator can be in any even-numbered position up to the eighth digit.      #   /// Determines if a code is valid.
   N   /// To be valid, all characters must be from the Open Location Code character
   N   /// set with at most one separator. The separator can be in any even-numbered
   %   /// position up to the eighth digit.
      ///
   6   /// - Parameter code: The Open Location Code to test.
   ?   /// - Returns: true if the code is a valid Open Location Code.
       �#&z   V  s:9IoTracker16OpenLocationCodeCy   Convert between decimal degree coordinates and plus codes. Shorten and recover plus codes for a given reference location.b      K   /// Convert between decimal degree coordinates and plus codes. Shorten and
   7   /// recover plus codes for a given reference location.
      ///
   J   /// Open Location Codes are short, 10-11 character codes that can be used
   Q   /// instead of street addresses. The codes can be generated and decoded offline,
   Q   /// and use a reduced character set that minimises the chance of codes including
      /// words.
      ///
   M   /// Codes are able to be shortened relative to a nearby location. This means
   N   /// that in many cases, only four to seven characters of the code are needed.
   P   /// To recover the original code, the same location is not required, as long as
   #   /// a nearby location is provided.
      ///
   M   /// Codes represent rectangular areas rather than points, and the longer the
   K   /// code, the smaller the area. A 10 character code represents a 13.5x13.5
   N   /// meter area (at the equator. An 11 character code represents approximately
      /// a 2.8x3.5 meter area.
      ///
   K   /// Two encoding algorithms are used. The first 10 characters are pairs of
   Q   /// characters, one for latitude and one for longitude, using base 20. Each pair
   P   /// reduces the area of the code by a factor of 400. Only even code lengths are
   P   /// sensible, since an odd-numbered length would have sides in a ratio of 20:1.
      ///
   M   /// At position 11, the algorithm changes so that each character selects one
   H   /// position from a 4x5 grid. This allows single-character refinements.
      ///
      /// # Swift Example
      /// ```
      /// import OpenLocationCode
      ///
      /// // ...
      ///
   3   /// // Encode a location with default code length.
   ?   /// if let code = OpenLocationCode.encode(latitude: 37.421908,
   D   ///                                       longitude: -122.084681) {
   +   ///   print("Open Location Code: \(code)")
      /// }
      ///
   4   /// // Encode a location with specific code length.
   F   /// if let code10Digit = OpenLocationCode.encode(latitude: 37.421908,
   I   ///                                              longitude: -122.084681,
   C   ///                                              codeLength: 10) {
   2   ///   print("Open Location Code: \(code10Digit)")
      /// }
      ///
      /// // Decode a full code:
   =   /// if let coord = OpenLocationCode.decode("849VCWC8+Q48") {
   K   ///   print("Center is \(coord.latitudeCenter), \(coord.longitudeCenter)")
      /// }
      ///
   9   /// // Attempt to trim the first characters from a code:
   F   /// if let shortCode = OpenLocationCode.shorten(code: "849VCWC8+Q48",
   @   ///                                             latitude: 37.4,
   E   ///                                             longitude: -122.0) {
   (   ///   print("Short code: \(shortCode)")
      /// }
      ///
   0   /// // Recover the full code from a short code:
   M   /// if let fullCode = OpenLocationCode.recoverNearest(shortcode: "CWC8+Q48",
   O   ///                                                   referenceLatitude: 37.4,
   T   ///                                                   referenceLongitude: -122.0) {
   0   ///   print("Recovered full code: \(fullCode)")
      /// }
      /// ```
      /// # Objective-C Examples
      /// ```
      /// @import OpenLocationCode;
      ///
      /// // ...
      ///
   3   /// // Encode a location with default code length.
   <   /// NSString *code = [OLCConverter encodeLatitude:37.421908
   @   ///                                     longitude:-122.084681];
   ,   /// NSLog(@"Open Location Code: %@", code);
      ///
   4   /// // Encode a location with specific code length.
   C   /// NSString *code10Digit = [OLCConverter encodeLatitude:37.421908
   E   ///                                            longitude:-122.084681
   >   ///                                           codeLength:10];
   3   /// NSLog(@"Open Location Code: %@", code10Digit);
      ///
      /// // Decode a full code:
   <   /// OLCArea *coord = [OLCConverter decode:@"849VCWC8+Q48"];
   Q   /// NSLog(@"Center is %.6f, %.6f", coord.latitudeCenter, coord.longitudeCenter);
      ///
   9   /// // Attempt to trim the first characters from a code:
   D   /// NSString *shortCode = [OLCConverter shortenCode:@"849VCWC8+Q48"
   9   ///                                        latitude:37.4
   =   ///                                       longitude:-122.0];
   )   /// NSLog(@"Short Code: %@", shortCode);
      ///
   0   /// // Recover the full code from a short code:
   T   /// NSString *recoveredCode = [OLCConverter recoverNearestWithShortcode:@"CWC8+Q48"
   M   ///                                                   referenceLatitude:37.4
   Q   ///                                                  referenceLongitude:-122.1];
   6   /// NSLog(@"Recovered Full Code: %@", recoveredCode);
      /// ```
      ///
    
    ��3   �   s:9IoTracker16OpenLocationCodeC6isFull4codeSbSS_tFZ             ///
   6   /// - Parameter code: The Open Location Code to test.
   >   /// - Returns: true if the code is a full Open Location Code.
       \)	"4   q   s:9IoTracker20OpenLocationCodeAreaV11longitudeHiSdvp*   The longitude of the NE corner in degrees.      /   /// The longitude of the NE corner in degrees.
        '3�e   �  s:9IoTracker16OpenLocationCodeC14recoverNearest9shortcode17referenceLatitude0I9LongitudeSSSgSS_S2dtFZA  Recover the nearest matching code to a specified location. Given a short Open Location Code of between four and seven characters, this recovers the nearest matching full code to the specified location. The number of characters that will be prepended to the short code, depends on the length of the short code and whether it starts with the separator. If it starts with the separator, four characters will be prepended. If it does not, the characters that will be prepended to the short code, where S is the supplied short code and R are the computed characters, are as follows:      ?   /// Recover the nearest matching code to a specified location.
   K   /// Given a short Open Location Code of between four and seven characters,
   L   /// this recovers the nearest matching full code to the specified location.
   O   /// The number of characters that will be prepended to the short code, depends
   N   /// on the length of the short code and whether it starts with the separator.
   N   /// If it starts with the separator, four characters will be prepended. If it
   O   /// does not, the characters that will be prepended to the short code, where S
   I   /// is the supplied short code and R are the computed characters, are as
      /// follows:
      /// ```
      /// SSSS  -> RRRR.RRSSSS
      /// SSSSS   -> RRRR.RRSSSSS
      /// SSSSSS  -> RRRR.SSSSSS
      /// SSSSSSS -> RRRR.SSSSSSS
      /// ```
      ///
   K   /// Note that short codes with an odd number of characters will have their
   @   /// last character decoded using the grid refinement algorithm.
      ///
   A   /// - Parameter shortcode: A valid short OLC character sequence.
   O   /// - Parameter referenceLatitude: The latitude (in signed decimal degrees) to
   2   ///   use to find the nearest matching full code.
   N   /// - Parameter referenceLongitude: The longitude (in signed decimal degrees)
   5   ///   to use to find the nearest matching full code.
   M   /// - Returns: The nearest full Open Location Code to the reference location
   L   ///   that matches the short code. If the passed code was not a valid short
   Q   ///   code, but was a valid full code, it is returned with proper capitalization
      ///   but otherwise unchanged.
        !���3   �   s:9IoTracker20OpenLocationCodeAreaV10codeLengthSivpX   The number of significant characters that were in the code. This excludes the separator.      @   /// The number of significant characters that were in the code.
   !   /// This excludes the separator.
        �Y��8   �   s:9IoTracker9AvroValueO6encodeySays5UInt8VGSgAA6SchemaOF   AvroValue binary encoding.      �   /**
    AvroValue binary encoding.

    :param schema Avro schema object.

    :returns Avro binary encoding as byte array. Nil if encoding fails.
    */       &t�a   �  s:9IoTracker16OpenLocationCodeC7shorten4code8latitude9longitude17maximumTruncationSSSgSS_S2dSitFZ�  Remove characters from the start of an OLC code. This uses a reference location to determine how many initial characters can be removed from the OLC code. The number of characters that can be removed depends on the distance between the code center and the reference location. The minimum number of characters that will be removed is four. If more than four characters can be removed, the additional characters will be replaced with the padding character. At most eight characters will be removed. The reference location must be within 50% of the maximum range. This ensures that the shortened code will be able to be recovered using slightly different locations.      5   /// Remove characters from the start of an OLC code.
   L   /// This uses a reference location to determine how many initial characters
   K   /// can be removed from the OLC code. The number of characters that can be
   N   /// removed depends on the distance between the code center and the reference
      /// location.
   K   /// The minimum number of characters that will be removed is four. If more
   K   /// than four characters can be removed, the additional characters will be
   J   /// replaced with the padding character. At most eight characters will be
   M   /// removed. The reference location must be within 50% of the maximum range.
   L   /// This ensures that the shortened code will be able to be recovered using
   "   /// slightly different locations.
      ///
   5   /// - Parameter code: A full, valid code to shorten.
   O   /// - Parameter latitude: A latitude, in signed decimal degrees, to use as the
      ///   reference point.
   M   /// - Parameter longitude: A longitude, in signed decimal degrees, to use as
      ///   the reference point.
   O   /// - Parameter maximumTruncation: The maximum number of characters to remove.
   K   /// - Returns: Either the original code, if the reference location was not
   %   ///   close enough, or the original.
       �^�+4   q   s:9IoTracker20OpenLocationCodeAreaV11longitudeLoSdvp*   The longitude of the SW corner in degrees.      /   /// The longitude of the SW corner in degrees.
        *UF;@   �   s:9IoTracker9AvroValueO6encodeySays5UInt8VGSg10Foundation4DataVF   AvroValue binary encoding.      �   /**
    AvroValue binary encoding.

    :param schemaData Avro JSON schema as a `Data` instance.

    :returns Avro binary encoding as byte array. Nil if encoding fails.
    */        q�\vS   �  s:9IoTracker16OpenLocationCodeC6encode8latitude9longitude10codeLengthSSSgSd_SdSitFZ�  Encode a location into an Open Location Code. Produces a code of the specified length, or the default length if no length is provided. The length determines the accuracy of the code. The default length is 10 characters, returning a code of approximately 13.5x13.5 meters. Longer codes represent smaller areas, but lengths > 14 are sub-centimetre and so 11 or 12 are probably the limit of useful codes.      2   /// Encode a location into an Open Location Code.
   I   /// Produces a code of the specified length, or the default length if no
      /// length is provided.
   J   /// The length determines the accuracy of the code. The default length is
   N   /// 10 characters, returning a code of approximately 13.5x13.5 meters. Longer
   N   /// codes represent smaller areas, but lengths > 14 are sub-centimetre and so
   5   /// 11 or 12 are probably the limit of useful codes.
      ///
   H   /// - Parameter latitude: A latitude in signed decimal degrees. Will be
   &   ///   clipped to the range -90 to 90.
   J   /// - Parameter longitude: A longitude in signed decimal degrees. Will be
   +   ///   normalised to the range -180 to 180.
   K   /// - Parameter codeLength: The number of significant digits in the output
   I   ///   code, not including any separator characters. Possible values are;
   I   ///   `2`, `4`, `6`, `8`, `10`, `11`, `12`, `13`, `14`, and `15`. Values
   A   ///   above `15` are accepted, but treated as `15`. Lower values
   A   ///   indicate a larger area, higher values a more precise area.
   G   ///   You can also shorten a code after encoding for codes used with a
   A   ///   reference point (e.g. your current location, a city, etc).
   <   /// - Returns: Open Location Code for the given coordinate.
        ��\8D   S   s:e:s:9IoTracker9AvroValueO6encodeySays5UInt8VGSg10Foundation4DataVF   Avro value binary encoding.          /// Avro value binary encoding.
         :��4   P  s:9IoTracker16OpenLocationCodeC7isShort4codeSbSS_tFZ�   Determines if a code is a valid short code. A short Open Location Code is a sequence created by removing four or more digits from an Open Location Code. It must include a separator character.      0   /// Determines if a code is a valid short code.
   N   /// A short Open Location Code is a sequence created by removing four or more
   C   /// digits from an Open Location Code. It must include a separator
      /// character.
      ///
   6   /// - Parameter code: The Open Location Code to test.
   ?   /// - Returns: true if the code is a short Open Location Code.
        {�]�   �   s:e:s:SS9IoTrackerE6lengthSivp8   Several extensions to String for character manipulation.      =   /// Several extensions to String for character manipulation.
          @         �                           �              �      S      Y
              	    c            �      �&  �(              �1                  3              =                          ;>                  �E              :F  �H                  "
h!               